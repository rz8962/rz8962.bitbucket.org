
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


function kreirajEHR() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var visina = $("#kreirajVisino").val();
	var teza = $("#kreirajTezo").val();
	var ehrId;

	if (!ime || !priimek || !visina || !teza || ime.trim().length == 0 || priimek.trim().length == 0 || visina.trim().length == 0 || teza.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr", 
		    type: 'POST',
		    success: function (data) {
		        ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            partyAdditionalInfo: [{key: "height", value: visina}, {key: "weight", value: teza}, {key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party", 
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {  
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'.</span>");
		                    console.log("Uspešno kreiran EHR '" + ehrId + "'.");
		                    $("#preberiEHRid").val(ehrId);
		                    
		                    
		                    var datumInUra = "2014-11-21T11:40Z";
							var telesnaVisina = visina;
							var telesnaTeza = teza;
							var telesnaTemperatura = "36.50";
							var sistolicniKrvniTlak = "118";
							var diastolicniKrvniTlak = "92";
							var nasicenostKrviSKisikom = "98";
							var merilec = "Roman Zalokar";
						
							if (!ehrId || ehrId.trim().length == 0) {
								$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
							} else {
								$.ajaxSetup({
								    headers: {"Ehr-Session": sessionId}
								});
								var podatki = {
								    "ctx/language": "en",
								    "ctx/territory": "SI",
								    "ctx/time": datumInUra,
								    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
								    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
								   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
								    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
								    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
								    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
								    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
								};
								var parametriZahteve = {
								    "ehrId": ehrId,
								    templateId: 'Vital Signs', 
								    format: 'FLAT',
								    committer: merilec
								};
								$.ajax({
								    url: baseUrl + "/composition?" + $.param(parametriZahteve),
								    type: 'POST',
								    contentType: 'application/json',
								    data: JSON.stringify(podatki),
								    success: function (res) { // pošiljanje podatkov na server
								    	console.log(res.meta.href);
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "' + dodani podatki.</span>");
								    },
								    error: function(err) {
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
										console.log(JSON.parse(err.responseText).userMessage);
								    }
								});
							}
		                    
		                }
		            },
		            error: function(err) {  // neuspešno
		            	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
		            	console.log(JSON.parse(err.responseText).userMessage);
		            }
		        });
		    }
		});
	}
}


function preberiEHR() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevan podatek!</span>");
	} else {
		
		var teza;
		var visina;
		
		
		var AQL = 
			
		"select " +
		"a_a/data[at0002]/events[at0003]/data[at0001]/items[at0004, 'Body weight']/value/magnitude as BWM " +
		"from EHR e[e/ehr_id/value='" + ehrId + "'] " +
		"contains OBSERVATION a_a[openEHR-EHR-OBSERVATION.body_weight.v1] " +
		"offset 0 limit 1";
				
		$.ajax({
		    url: baseUrl + "/query?" + $.param({"aql": AQL}),
		    type: 'GET',
		    headers: {"Ehr-Session": sessionId},
		    success: function (res) {
		    	
		    	//console.log(res.resultSet[0]);
		    	teza = res.resultSet[0].BWM;
		    	

		    },
		    error: function() {
		    	alert("Napaka v poizvedbi");
		    }
		});
		 
        $.ajax({
            url: baseUrl + "/view/" + ehrId + "/height",
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId
            },
            success: function (data) {
                visina = data[0].height;
                //console.log(data);
            }
        });
		
		
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				patientBMI = Math.round(teza / (visina/100 * visina/100));
				
				if(patientBMI > 25){
				$("#pacientov-bmi").html("<br><br>Pacientov BMI je: <b style='font-size:50px;color:red;'>" + patientBMI + "</b>.");
					$("#prehrana-tekst").html("<br><img src='fat.jpg'>");}
				else if(patientBMI<18){
					$("#pacientov-bmi").html("<br><br>Pacientov BMI je: <b style='font-size:50px;color:red;'>" + patientBMI + "</b>.");
					$("#prehrana-tekst").html("<br><img src='skinny.jpeg'>");}
					
				else{
					$("#pacientov-bmi").html("<br><br>Pacientov BMI je: <b style='font-size:50px;color:green;'>" + patientBMI + "</b>.");
					$("#prehrana-tekst").html("<br><img src='perfect.jpg'>");}
					
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
				console.log(JSON.parse(err.responseText).userMessage);
			}
		});
	}	
}


 

  
        //


$(document).ready(function() {
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});
	$('#preberiPredlogoBolnika').change(function() {
		$("#kreirajSporocilo").html("");
		var podatki = $(this).val().split(",");
		$("#kreirajIme").val(podatki[0]);
		$("#kreirajPriimek").val(podatki[1]);
		$("#kreirajVisino").val(podatki[2]);
		$("#kreirajTezo").val(podatki[3]);
	});
	
});